package br.com.saip.util;

public abstract interface Constantes {
	
    public static enum TipoUsuario  {
    	ADMINISTRADOR("Administrador"),
    	IML("IML"),
    	LEGISTA("Legista"),
    	DENTISTA("Dentista");
		
		private final String label;
		
		private TipoUsuario(String lab) {
			this.label = lab;
		}
		
		public String getLabel() {
			return label;
		}
	};

    public static enum Sexo{
    	MASCULINO("Masculino"),
    	FEMININO("Feminino"),
    	INDEFINIDO("Indefinido");
		
		private final String label;
		
		private Sexo(String lab) {
			this.label = lab;
		}
		
		public String getLabel() {
			return label;
		}
	};
	
	public static enum Status {
		DESAPARECIDO("Desaparecido"),
		ENCONTRADO("Encontrado"),
		IDENTIFICADO("Identificado"),
		LIBERADO("Liberado");
		
		private final String label;
		
		private Status(String lab) {
			this.label = lab;
		}
		
		public String getLabel() {
			return label;
		}
	}
}