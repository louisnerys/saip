package br.com.saip.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;

import br.com.saip.util.Constantes;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NamedQueries({
		@NamedQuery(
				name = "Pessoa.getListaOdontogramaDesaparecidoAssociado",
				query = "from Desaparecido where responsavel = :responsavel and status = :status order by numero"),
		@NamedQuery(
				name = "Pessoa.getListaOdontogramaCadaver",
				query = "from Cadaver as c left join fetch c.encontrado d where d.id is null order by c.numero"),
		@NamedQuery(
				name = "Pessoa.getListaDesaparecidoNaoEncontrado",
				query = "from Desaparecido where status = :status order by numero"),
		@NamedQuery(
				name = "Pessoa.getListaCadaverNaoEncontrado",
				query = "from Cadaver as c left join fetch c.encontrado d where d.id is null order by c.numero"),
		@NamedQuery(
				name = "Pessoa.getListaCadaverIdentificado",
				query = "from Cadaver as c inner join fetch c.encontrado e where e.status = :status order by c.numero") })
public abstract class Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator = "tableGenerator")
	@TableGenerator(name = "tableGenerator", table = "sequences")
	private int id;

	@Column(nullable = false, unique=true)
	private int numero;
	
	@Enumerated(value = EnumType.ORDINAL)
	private Constantes.Sexo sexo;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.EAGER)
	@JoinColumn
	private Set<Foto> fotos;

	@OneToOne(mappedBy = "pessoa", cascade = CascadeType.ALL)
	private Odontograma odontograma;

	private String nome;
	private String CPF;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date dataNascimento;
	private String RG;
	private String emissor;
	@ManyToOne
	private Estado estado;
	private String cidade;
	private String bairro;
	private String logradouro;
	private Integer numeroCasa;
	private String complemento;

	public Odontograma getOdontograma() {
		if(odontograma == null)
			odontograma = new Odontograma();
		return odontograma;
	}

	public void setOdontograma(Odontograma odontograma) {
		this.odontograma = odontograma;
	}

	public Set<Foto> getFotos() {
		if(fotos == null)
			fotos = new HashSet<Foto>();
		return fotos;
	}

	public void setFotos(Set<Foto> fotos) {
		this.fotos = fotos;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Constantes.Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Constantes.Sexo sexo) {
		this.sexo = sexo;
	}

	public String getCPF() {
		return CPF;
	}

	public void setCPF(String CPF) {
		if(CPF == null || CPF.trim().equals(""))
			CPF = null;
		this.CPF = CPF;
	}

	public String getRG() {
		return RG;
	}

	public void setRG(String RG) {
		this.RG = RG;
	}

	public String getEmissor() {
		return emissor;
	}

	public void setEmissor(String emissor) {
		this.emissor = emissor;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getNumeroCasa() {
		return numeroCasa;
	}

	public void setNumeroCasa(Integer numeroCasa) {
		this.numeroCasa = numeroCasa;
	}
	
	public boolean isOdontogramaVazio() {
		return getOdontograma().getDentes().isEmpty();
	}

	public Set<String> getOdontogramaAsString() {
		Set<String> lista = new HashSet<String>();

		if (getOdontograma() != null && getOdontograma().getDentes() != null)
			for (Dente dente : getOdontograma().getDentes()) {
				if (dente.isAusente()) {
					lista.add("0" + dente.getNumero() + "0" + "0000");
					continue;
				}
				for (Face face : dente.getFaces()) {
					for (Caracteristica caracteristica : face
							.getCaracteristicas()) {
						lista.add("1" + dente.getNumero() + face.getNumero()
								+ String.format("%4d", caracteristica.getId()).replace(' ', '0'));
					}
				}
			}

		return lista;
	}
	
	public double comparaPorOdontograma(Pessoa outro) {
		Set<String> uniao = new HashSet<String>(getOdontogramaAsString());
		uniao.addAll(outro.getOdontogramaAsString());
		
		Set<String> intersecao = new HashSet<String>(getOdontogramaAsString());
		intersecao.retainAll(outro.getOdontogramaAsString());
		
		if (uniao.size() == 0)
			if (intersecao.size() == 0)
				return 100;
			else
				return 0;
		
		return new Double(intersecao.size()) / new Double(uniao.size()) * 100;
	}
}