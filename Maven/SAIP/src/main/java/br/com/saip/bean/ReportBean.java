package br.com.saip.bean;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

@ManagedBean
@RequestScoped
public class ReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{identificacaoBean}")
	private IdentificacaoBean identificacao;

	public void setIdentificacao(IdentificacaoBean ident) {
		identificacao = ident;
	}

	@ManagedProperty(value = "#{loginBean}")
	private LoginBean loginBean;

	public void setLoginBean(LoginBean login) {
		loginBean = login;
	}

	public void emiteLiberacao() throws IOException, SQLException {

		RelatorioLiberacao relatorio = new RelatorioLiberacao();

		relatorio.setNumeroCadaver(identificacao.getPessoa().getNumero());
		relatorio.setNomeCadaver(identificacao.getPessoa().getNome());
		relatorio.setNumeroDesaparecido(identificacao.getPossivel().getNumero());
		relatorio.setNomeDesaparecido(identificacao.getPossivel().getNome());
		relatorio.setNomeLegista(loginBean.getUsuario().getNome());
		relatorio.setCfoLegista(loginBean.getUsuario().getDentista().getCFO());
		relatorio.setNomeIml(loginBean.getUsuario().getIml().getCidade()
				.toUpperCase()
				+ " - "
				+ loginBean.getUsuario().getIml().getEstado().getSigla()
						.toUpperCase());

		List<RelatorioLiberacao> l = new ArrayList<RelatorioLiberacao>();

		l.add(relatorio);

		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(l);

		Map<String, Object> parameters = new HashMap<String, Object>();

		try {

			FacesContext facesContext = FacesContext.getCurrentInstance();

			facesContext.responseComplete();

			ServletContext scontext = (ServletContext) facesContext
					.getExternalContext().getContext();

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					scontext.getRealPath("/WEB-INF/report/libera.jasper"),
					parameters, ds);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRPdfExporter exporter = new JRPdfExporter();
			
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			byte[] bytes = baos.toByteArray();

			if (bytes != null && bytes.length > 0) {

				HttpServletResponse response = (HttpServletResponse) facesContext
						.getExternalContext().getResponse();
				response.setContentType("application/pdf");
				response.setHeader("Content-disposition",
						"inline; filename=\"liberacao.pdf\"");
				response.setContentLength(bytes.length);
				ServletOutputStream outputStream = response.getOutputStream();
				outputStream.write(bytes, 0, bytes.length);
				outputStream.flush();
				outputStream.close();
			}

		} catch (Exception e) {

			Logger.getLogger(getClass()).error(e);
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
							"Ocorreu um erro ao processar."));
			
			e.printStackTrace();

		}

		identificacao.liberar();

	}

}
