package br.com.saip.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.saip.dao.EstadoDao;
import br.com.saip.dao.IMLDao;
import br.com.saip.model.Estado;
import br.com.saip.model.IML;

@ManagedBean(name="IMLBean")
@ViewScoped
public class IMLBean extends GenericBean<IML> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Estado> estados;
	
	private int estado;

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	@Override
    public List<IML> getLista() {
        setLista(new IMLDao().list());
        return this.lista;
    }
	
	@Override
	public void prepararAlterar() {
		super.prepararAlterar();
		estado = getObjeto().getEstado().getId();
	}

    @Override
    public void prepararAdicionar() {
        setObjeto(new IML());
        setEstado(0);
    }

	public List<Estado> getEstados() {
		estados = new EstadoDao().list();
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}
	
	@Override
	public void confirmar(ActionEvent actionEvent) {
		getObjeto().setEstado(new EstadoDao().getEstado(estado));
		super.confirmar(actionEvent);
	}
}