package br.com.saip.dao;

import br.com.saip.model.Telefone;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class TelefoneDao extends Dao<Telefone> {
    public Telefone getTelefone(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Telefone)session.load(Telefone.class, id);
    }

    public List<Telefone> list(){
        return list(" from Telefone");
    }
}
