package br.com.saip.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.saip.dao.EstadoDao;
import br.com.saip.model.Estado;

@ManagedBean
@ViewScoped
public class EstadoBean extends GenericBean<Estado> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Override
    public List<Estado> getLista() {
        setLista(new EstadoDao().list());
        return this.lista;
    }

    @Override
    public void prepararAdicionar() {
        setObjeto(new Estado());
    }
}