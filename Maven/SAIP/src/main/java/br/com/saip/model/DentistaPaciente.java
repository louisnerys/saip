package br.com.saip.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="DentistaPaciente")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class DentistaPaciente extends Dentista implements Serializable {
	private static final long serialVersionUID = 1L;
}
