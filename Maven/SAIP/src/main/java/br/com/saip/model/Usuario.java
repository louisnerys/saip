package br.com.saip.model;

import java.io.Serializable;
import java.security.MessageDigest;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.DatatypeConverter;

import br.com.saip.util.Constantes;

@NamedQueries({
	@NamedQuery(
			name = "Usuario.getUsuarioPorUsername",
			query = "from Usuario u where u.username = :username"
			)
})
@Entity
@Table(name="Usuario")
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private long id;

    @Column(nullable=false)
    private String email;
    private String nome;

    private String username;
	private String senha;
    private Constantes.TipoUsuario tipoUsuario;
    
    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER, optional=true, mappedBy="usuario", orphanRemoval=true)
    private Dentista dentista;
    
    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    private IML iml;

    public Constantes.TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(Constantes.TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
    	MessageDigest md = null;
    	
    	try
    	{
    		md = MessageDigest.getInstance("SHA");
    		md.update(senha.getBytes("UTF-8"));
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
     
        byte raw[] = md.digest();
        String hash = DatatypeConverter.printBase64Binary(raw);
        
        this.senha = hash;
    }

	public Dentista getDentista() {
		return dentista;
	}

	public void setDentista(Dentista dentista) {
		this.dentista = dentista;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public IML getIml() {
		return iml;
	}

	public void setIml(IML iml) {
		this.iml = iml;
	}

    public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public boolean isDentista() {
		return tipoUsuario == Constantes.TipoUsuario.DENTISTA ||
				tipoUsuario == Constantes.TipoUsuario.LEGISTA;
	}
}