package br.com.saip.dao;

import br.com.saip.model.Foto;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class FotoDao extends Dao<Foto> {
    public Foto getFoto(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Foto)session.load(Foto.class, id);
    }

    public List<Foto> list(){
        return list(" from Foto");
    }
}
