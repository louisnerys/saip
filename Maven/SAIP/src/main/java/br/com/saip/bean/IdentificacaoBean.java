package br.com.saip.bean;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import br.com.saip.dao.Dao;
import br.com.saip.dao.PessoaDao;
import br.com.saip.model.Cadaver;
import br.com.saip.model.Desaparecido;
import br.com.saip.model.Pessoa;
import br.com.saip.util.Constantes;

@ManagedBean(name="identificacaoBean")
@SessionScoped
public class IdentificacaoBean implements
		Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private List<Pessoa> filtro;

	private Pessoa pessoa;
	
	private Pessoa possivel;
	
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Pessoa getPossivel() {
		return possivel;
	}

	public void setPossivel(Pessoa possivel) {
		this.possivel = possivel;
	}

	public List<Pessoa> getFiltro() {
		return filtro;
	}

	public void setFiltro(List<Pessoa> filtro) {
		this.filtro = filtro;
	}

	public List<Cadaver> getLista() {
		return PessoaDao.getCadaveresNaoIdentificados();
	}

	public List<Cadaver> getListaIdentificado() {
		return PessoaDao.getCadaveresIdentificados();
	}

	public List<Cadaver> getListaLiberado() {
		return PessoaDao.getCadaveresLiberados();
	}

	public List<Desaparecido> getListaPossiveis() {
		return PessoaDao.getDesaparecidosNaoIdentificados();
	}
	
	public String associar() {
		try
    	{
			if(pessoa instanceof Cadaver) {
				((Cadaver) pessoa).setEncontrado((Desaparecido) possivel);
				((Desaparecido) possivel).setEncontrado((Cadaver) pessoa);
				((Desaparecido) possivel).setStatus(Constantes.Status.IDENTIFICADO);
			}
			else if(pessoa instanceof Desaparecido) {
				((Cadaver) possivel).setEncontrado((Desaparecido) pessoa);
				((Desaparecido) pessoa).setEncontrado((Cadaver) possivel);
				((Desaparecido) pessoa).setStatus(Constantes.Status.IDENTIFICADO);
			}
			
	    	new Dao<Pessoa>(){}.update(getPessoa());
	    	new Dao<Pessoa>(){}.update(getPossivel());
    	}
    	catch( Exception e )
    	{
    		Logger.getLogger(getClass()).error(e);
    		FacesContext.getCurrentInstance().addMessage(null,
    				new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
    						"Ocorreu um erro ao processar."));
    		return null;
    	}
    	
		FacesContext.getCurrentInstance().addMessage(null,
    			new FacesMessage(FacesMessage.SEVERITY_INFO, "",
    					"Opera&ccedil;&atilde;o realizada com sucesso!"));
    	
		return "index.xhtml";
	}
	
	public String liberar() {
		try
    	{
			if(pessoa instanceof Cadaver) {
				((Desaparecido) possivel).setStatus(Constantes.Status.LIBERADO);
				new Dao<Pessoa>(){}.update(getPossivel());
			}
			else if(pessoa instanceof Desaparecido) {
				((Desaparecido) pessoa).setStatus(Constantes.Status.LIBERADO);
				new Dao<Pessoa>(){}.update(getPessoa());
			}
			
    	}
    	catch( Exception e )
    	{
    		Logger.getLogger(getClass()).error(e);
    		FacesContext.getCurrentInstance().addMessage(null,
    				new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
    						"Ocorreu um erro ao processar."));
    		return null;
    	}
    	
		FacesContext.getCurrentInstance().addMessage(null,
    			new FacesMessage(FacesMessage.SEVERITY_INFO, "",
    					"Opera&ccedil;&atilde;o realizada com sucesso!"));
		
		return null;
	}
	
	private String getNomeExibicao(Pessoa pessoa) {
		StringBuilder sb = new StringBuilder();
		
		if (pessoa instanceof Cadaver)
			sb.append("Cad&aacute;ver: ");
		else
			sb.append("Desaparecido: ");
		
		sb.append(new DecimalFormat("#,##0").format(pessoa.getNumero()));
		
		sb.append(" - ");
		sb.append(pessoa.getNome());
		
		return sb.toString();
	}
	
	public String getNomePessoa() {
		return getNomeExibicao(pessoa);
	}
	
	public String getNomePossivel() {
		return getNomeExibicao(possivel);
	}
}