package br.com.saip.dao;

import br.com.saip.model.DentistaPaciente;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class DentistaPacienteDao extends Dao<DentistaPaciente> {
    public DentistaPaciente getDentistaPaciente(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (DentistaPaciente)session.load(DentistaPaciente.class, id);
    }

    public List<DentistaPaciente> list(){
        return list(" from DentistaPaciente");
    }

}
