package br.com.saip.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Dente")
public class Dente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private int id;

    @ManyToOne
    private Odontograma odontograma;

    @Column(nullable = false)
    private int numero;
    private boolean ausente;
    private String observacao;

    @OneToMany(fetch=FetchType.EAGER, orphanRemoval=true, mappedBy="dente", cascade=CascadeType.ALL)
    private Set<Face> faces;

	public Set<Face> getFaces() {
		if(faces == null)
			faces = new HashSet<Face>();
        return faces;
    }

    public void setFaces(Set<Face> faces) {
		this.faces = faces;
	}

    public boolean isAusente() {
        return ausente;
    }

    public void setAusente(boolean ausente) {
        this.ausente = ausente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Odontograma getOdontograma() {
        return odontograma;
    }

    public void setOdontograma(Odontograma odontograma) {
        this.odontograma = odontograma;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numero;
		result = prime * result
				+ ((odontograma == null) ? 0 : odontograma.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dente other = (Dente) obj;
		if (numero != other.numero)
			return false;
		if (odontograma == null) {
			if (other.odontograma != null)
				return false;
		} else if (!odontograma.equals(other.odontograma))
			return false;
		return true;
	}
}