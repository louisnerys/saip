package br.com.saip.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import br.com.saip.model.Telefone;

@ManagedBean
@ViewScoped
public class TelefoneBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean novo;
    private Telefone objeto;
    protected Set<Telefone> telefones;

    public boolean isNovo() {
        return novo;
    }

    public void setNovo(boolean novo) {
        this.novo = novo;
    }

    public Telefone getObjeto() {
        return objeto;
    }

    public void setObjeto(Telefone objeto) {
        this.objeto = objeto;
    }

    public Set<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(Set<Telefone> telefones) {
		this.telefones = telefones;
	}

	public List<Telefone> getLista() {
    	return telefones == null ? null : new ArrayList<Telefone>(telefones);
    }

	public void prepararAdicionar(ActionEvent actionEvent)
    {
        setNovo(true);
        prepararAdicionar();
    }

    public void prepararAdicionar() {
    	objeto = new Telefone();
    }

    public void prepararAlterar() {
        setNovo(false);
    }

    public  void excluir() {
        telefones.remove(objeto);
        FacesContext.getCurrentInstance().addMessage(null,
        		new FacesMessage(FacesMessage.SEVERITY_INFO, "",
        				"Opera&ccedil;&atilde;o realizada com sucesso!"));
    }

    public void confirmar(ActionEvent actionEvent) {
    	try
    	{
	    	telefones.add(objeto);
    	}
    	catch( Exception e )
    	{
    		e.printStackTrace();
    		FacesContext.getCurrentInstance().addMessage(null,
    				new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
    						"Ocorreu um erro ao processar."));
    		return;
    	}
    	FacesContext.getCurrentInstance().addMessage(null,
    			new FacesMessage(FacesMessage.SEVERITY_INFO, "",
    					"Opera&ccedil;&atilde;o realizada com sucesso!"));
    }
}