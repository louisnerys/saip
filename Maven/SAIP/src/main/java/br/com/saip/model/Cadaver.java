package br.com.saip.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name="Cadaver")
public class Cadaver extends Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;
	private boolean indigente;
    private String tipoAcidente;
    private String localAcidente;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataAcidente;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataLocalizacao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataObito;
    private String descricaoEspecifica;
    private String achadosAdicionais;
    private String examesAdicionais;
    private int idadeEstimada;
    private String metodo;
    
    @OneToOne(mappedBy="encontrado", cascade=CascadeType.MERGE)
    private Desaparecido encontrado;

    public Desaparecido getEncontrado() {
		return encontrado;
	}

	public void setEncontrado(Desaparecido encontrado) {
		this.encontrado = encontrado;
	}

	public String getAchadosAdicionais() {
        return achadosAdicionais;
    }

    public void setAchadosAdicionais(String achadosAdicionais) {
        this.achadosAdicionais = achadosAdicionais;
    }

    public Date getDataAcidente() {
        return dataAcidente;
    }

    public void setDataAcidente(Date dataAcidente) {
        this.dataAcidente = dataAcidente;
    }

    public Date getDataLocalizacao() {
        return dataLocalizacao;
    }

    public void setDataLocalizacao(Date dataLocalizacao) {
        this.dataLocalizacao = dataLocalizacao;
    }

    public Date getDataObito() {
        return dataObito;
    }

    public void setDataObito(Date dataObito) {
        this.dataObito = dataObito;
    }

    public String getDescricaoEspecifica() {
        return descricaoEspecifica;
    }

    public void setDescricaoEspecifica(String descricaoEspecifica) {
        this.descricaoEspecifica = descricaoEspecifica;
    }

    public String getExamesAdicionais() {
        return examesAdicionais;
    }

    public void setExamesAdicionais(String examesAdicionais) {
        this.examesAdicionais = examesAdicionais;
    }

    public int getIdadeEstimada() {
        return idadeEstimada;
    }

    public void setIdadeEstimada(int idadeEstimada) {
        this.idadeEstimada = idadeEstimada;
    }

    public boolean isIndigente() {
        return indigente;
    }

    public void setIndigente(boolean indigente) {
        this.indigente = indigente;
        if(indigente) {
        	setNome(null);
        	setCPF(null);
        	setRG(null);
        	setEmissor(null);
        	setDataNascimento(null);
        	setEstado(null);
        	setCidade(null);
        	setBairro(null);
        	setLogradouro(null);
        	setNumeroCasa(null);
        	setComplemento(null);
        }
    }

    public String getLocalAcidente() {
        return localAcidente;
    }

    public void setLocalAcidente(String localAcidente) {
        this.localAcidente = localAcidente;
    }

    public String getMetodo() {
        return metodo;
    }

    public void setMetodo(String metodo) {
        this.metodo = metodo;
    }

    public String getTipoAcidente() {
        return tipoAcidente;
    }

    public void setTipoAcidente(String tipoAcidente) {
        this.tipoAcidente = tipoAcidente;
    }
    
    @Override
    public String getNome() {
    	if (isIndigente())
    		return "(Indigente)";
    	return super.getNome();
    }
    
    @Override
    public void setNome(String nome) {
    	if(isIndigente())
    		nome = null;
    	super.setNome(nome);
    }
    
    @Override
    public void setCPF(String CPF) {
    	if(isIndigente())
    		CPF = null;
    	super.setCPF(CPF);
    }

	public void setRG(String RG) {
    	if(isIndigente())
    		RG = null;
		super.setRG(RG);
	}
	
	@Override
	public void setEmissor(String emissor) {
    	if(isIndigente())
    		emissor = null;
		super.setEmissor(emissor);
	}

	public void setBairro(String bairro) {
    	if(isIndigente())
    		bairro = null;
		super.setBairro(bairro);
	}

	public void setCidade(String cidade) {
    	if(isIndigente())
    		cidade = null;
		super.setCidade(cidade);
	}

	public void setComplemento(String complemento) {
    	if(isIndigente())
    		complemento = null;
		super.setComplemento(complemento);
	}

	public void setDataNascimento(Date dataNascimento) {
    	if(isIndigente())
    		dataNascimento = null;
		super.setDataNascimento(dataNascimento);
	}

	public void setEstado(Estado estado) {
    	if(isIndigente())
    		estado = null;
		super.setEstado(estado);
	}

	public void setLogradouro(String logradouro) {
    	if(isIndigente())
    		logradouro = null;
		super.setLogradouro(logradouro);
	}

	public void setNumeroCasa(Integer numeroCasa) {
    	if(isIndigente())
    		numeroCasa = null;
		super.setNumeroCasa(numeroCasa);
	}
}