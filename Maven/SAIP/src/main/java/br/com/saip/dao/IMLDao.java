package br.com.saip.dao;

import br.com.saip.model.IML;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class IMLDao extends Dao<IML> {
	
    public static IML getIML(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (IML)session.load(IML.class, id);
    }

    public List<IML> list(){
        return list(" from IML");
    }
}