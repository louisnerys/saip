package br.com.saip.util;

import org.hibernate.SessionFactory;
import org.hibernate.SessionFactoryObserver;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import br.com.saip.model.Cadaver;
import br.com.saip.model.Caracteristica;
import br.com.saip.model.Dente;
import br.com.saip.model.Dentista;
import br.com.saip.model.DentistaPaciente;
import br.com.saip.model.Desaparecido;
import br.com.saip.model.Estado;
import br.com.saip.model.Face;
import br.com.saip.model.Foto;
import br.com.saip.model.IML;
import br.com.saip.model.OdontoLegista;
import br.com.saip.model.Odontograma;
import br.com.saip.model.Pessoa;
import br.com.saip.model.Telefone;
import br.com.saip.model.Usuario;

public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
			Configuration ac = new Configuration();
			
			ac.configure();
			
            ac.addAnnotatedClass(Cadaver.class);
            ac.addAnnotatedClass(Caracteristica.class);
            ac.addAnnotatedClass(Dente.class);
            ac.addAnnotatedClass(Dentista.class);
            ac.addAnnotatedClass(DentistaPaciente.class);
            ac.addAnnotatedClass(Desaparecido.class);
            ac.addAnnotatedClass(Estado.class);
            ac.addAnnotatedClass(Face.class);
            ac.addAnnotatedClass(Foto.class);
            ac.addAnnotatedClass(IML.class);
            ac.addAnnotatedClass(OdontoLegista.class);
            ac.addAnnotatedClass(Odontograma.class);
            ac.addAnnotatedClass(Pessoa.class);
            ac.addAnnotatedClass(Telefone.class);
            ac.addAnnotatedClass(Usuario.class);
            
            serviceRegistry = new ServiceRegistryBuilder().applySettings(
                    ac.getProperties()).buildServiceRegistry();
           
            ac.setSessionFactoryObserver(new SessionFactoryObserver() {
				
				private static final long serialVersionUID = 1L;
     
                public void sessionFactoryCreated(SessionFactory factory) { }
     
                public void sessionFactoryClosed(SessionFactory factory) {
                    ServiceRegistryBuilder.destroy(serviceRegistry);
                }
            });
            
            sessionFactory = ac.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Criação da SessionFactory falhou" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
