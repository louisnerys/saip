package br.com.saip.dao;

import br.com.saip.model.Odontograma;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class OdontogramaDao extends Dao<Odontograma> {
    public Odontograma getOdontograma(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Odontograma)session.load(Odontograma.class, id);
    }

    public List<Odontograma> list(){
        return list(" from Odontograma");
    }
}
