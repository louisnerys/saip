package br.com.saip.dao;

import br.com.saip.model.Dentista;
import br.com.saip.model.DentistaPaciente;
import br.com.saip.model.OdontoLegista;
import br.com.saip.model.Usuario;
import br.com.saip.util.Constantes;
import br.com.saip.util.HibernateUtil;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UsuarioDao extends Dao<Usuario> {
    public Usuario getUsuario(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Usuario)session.load(Usuario.class, id);
    }
    
    public static Usuario getUsuarioPorUsername(String username) {
    	Usuario usuario = null;
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	Query query = session.getNamedQuery("Usuario.getUsuarioPorUsername").setString("username", username);
    	@SuppressWarnings("unchecked")
		List<Usuario> l = query.list();
    	if(!l.isEmpty())
    		usuario = l.get(0);
		session.close();
		return usuario;
    }

    public List<Usuario> list(){
        return list(" from Usuario");
    }
    
    public void merge(Usuario usuario, Dentista dentista)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        
    	if(usuario.isDentista())
		{
			if(usuario.getDentista() != null)
			{
				dentista = usuario.getDentista();
				String cfo = dentista.getCFO();
				if(dentista instanceof OdontoLegista &&
						usuario.getTipoUsuario().equals(Constantes.TipoUsuario.DENTISTA))
				{
					excluiDentista(usuario, dentista, session);
					dentista = new DentistaPaciente();
					dentista.setCFO(cfo);
				}
				else if(dentista instanceof DentistaPaciente &&
						usuario.getTipoUsuario().equals(Constantes.TipoUsuario.LEGISTA))
				{
					excluiDentista(usuario, dentista, session);
					dentista = new OdontoLegista();
					dentista.setCFO(cfo);
				}
			}
			dentista.setUsuario(usuario);
			usuario.setDentista(dentista);
		}
		else if(usuario.getDentista() != null)
			excluiDentista(usuario, dentista, session);
    	
        session.merge(usuario);
        session.flush();
        t.commit();
        session.close();
    }
    
    private void excluiDentista(Usuario usuario, Dentista dentista, Session session) {
    	usuario.getDentista().setUsuario(null);
		usuario.setDentista(null);
		session.delete(dentista);
    }
}
