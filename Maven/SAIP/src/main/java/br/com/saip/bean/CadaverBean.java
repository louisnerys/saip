package br.com.saip.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import br.com.saip.dao.CadaverDao;
import br.com.saip.dao.DesaparecidoDao;
import br.com.saip.dao.EstadoDao;
import br.com.saip.model.Cadaver;
import br.com.saip.model.Estado;
import br.com.saip.model.Foto;
import br.com.saip.util.Constantes;

@ManagedBean
@SessionScoped
public class CadaverBean extends GenericBean<Cadaver> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{fotoBean}")
	private FotoBean fotoBean;

	private int estado;
	
	private Foto foto;
	
	private Constantes.Sexo sexo;

	@Override
    public List<Cadaver> getLista() {
        setLista(new CadaverDao().list());
        return this.lista;
    }

    @Override
    public void prepararAdicionar() {
        setObjeto(new Cadaver());
        setEstado(0);
        setSexo(Constantes.Sexo.MASCULINO);
    }
	
	@Override
	public void prepararAlterar() {
		super.prepararAlterar();
		if(getObjeto().getEstado() == null)
			setEstado(0);
		else
			setEstado(getObjeto().getEstado().getId());
		setSexo(getObjeto().getSexo());
	}
	
	@Override
	public void confirmar(ActionEvent actionEvent) {
		if(estado > 0)
			getObjeto().setEstado(new EstadoDao().getEstado(estado));
		getObjeto().setSexo(sexo);
		super.confirmar(actionEvent);
	}
	
	public void adicionaFoto() {
		setFoto(fotoBean.getFoto());
		getObjeto().getFotos().add(getFoto());
		super.confirmar(null);
	}
	
	public void excluirFoto() {
		getObjeto().getFotos().remove(getFoto());
		super.confirmar(null);
	}
	
	public List<Foto> getFotos() {
		if(getObjeto() == null)
			return Collections.emptyList();
		return new ArrayList<Foto>(getObjeto().getFotos());
	}
	
	public void reconhecido() {
		getObjeto().getEncontrado().setStatus(Constantes.Status.ENCONTRADO);
		super.confirmar(null);
	}
	
	public void naoReconhecido() {
		if(getObjeto().getEncontrado() != null) {
			getObjeto().getEncontrado().setEncontrado(null);
			getObjeto().getEncontrado().setStatus(Constantes.Status.DESAPARECIDO);
			new DesaparecidoDao().update(getObjeto().getEncontrado());
			getObjeto().setEncontrado(null);
		}
		super.confirmar(null);
	}
    
    public List<Estado> getEstados() {
    	return new EstadoDao().list();
    }

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Constantes.Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Constantes.Sexo sexo) {
		this.sexo = sexo;
	}
	
	public int getTabIndex() {
		return 0;
	}
	
	public void setTabIndex(int tabInd) { }

	public Foto getFoto() {
		return foto;
	}

	public void setFoto(Foto foto) {
		this.foto = foto;
	}
	
	public void setFotoBean(FotoBean fb) {
		fotoBean = fb;
	}
}