package br.com.saip.bean;

public class RelatorioLiberacao {
	private int numeroCadaver;
	private String nomeCadaver;
	private int numeroDesaparecido;
	private String nomeDesaparecido;
	private String nomeLegista;
	private String cfoLegista;
	private String nomeIml;

	public RelatorioLiberacao() { }

	public int getNumeroCadaver() {
		return numeroCadaver;
	}

	public void setNumeroCadaver(int numeroCadaver) {
		this.numeroCadaver = numeroCadaver;
	}

	public String getNomeCadaver() {
		return nomeCadaver;
	}

	public void setNomeCadaver(String nomeCadaver) {
		this.nomeCadaver = nomeCadaver;
	}

	public int getNumeroDesaparecido() {
		return numeroDesaparecido;
	}

	public void setNumeroDesaparecido(int numeroDesaparecido) {
		this.numeroDesaparecido = numeroDesaparecido;
	}

	public String getNomeDesaparecido() {
		return nomeDesaparecido;
	}

	public void setNomeDesaparecido(String nomeDesaparecido) {
		this.nomeDesaparecido = nomeDesaparecido;
	}

	public String getNomeLegista() {
		return nomeLegista;
	}

	public void setNomeLegista(String nomeLegista) {
		this.nomeLegista = nomeLegista;
	}

	public String getCfoLegista() {
		return cfoLegista;
	}

	public void setCfoLegista(String cfoLegista) {
		this.cfoLegista = cfoLegista;
	}

	public String getNomeIml() {
		return nomeIml;
	}

	public void setNomeIml(String nomeIml) {
		this.nomeIml = nomeIml;
	}

}