package br.com.saip.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Face")
public class Face implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private int numero;

    @ManyToOne
    private Dente dente;

    @ManyToMany(cascade=CascadeType.REFRESH, fetch=FetchType.EAGER)
    private Set<Caracteristica> caracteristicas;

    public Set<Caracteristica> getCaracteristicas() {
    	if (caracteristicas == null)
    		caracteristicas = new HashSet<Caracteristica>();
        return caracteristicas;
    }

    public void setCaracteristicas(Set<Caracteristica> caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public Dente getDente() {
        return dente;
    }

    public void setDente(Dente dente) {
        this.dente = dente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dente == null) ? 0 : dente.hashCode());
		result = prime * result + numero;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Face other = (Face) obj;
		if (dente == null) {
			if (other.dente != null)
				return false;
		} else if (!dente.equals(other.dente))
			return false;
		if (numero != other.numero)
			return false;
		return true;
	}
}