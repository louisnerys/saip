package br.com.saip.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Odontograma")
public class Odontograma implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private int id;

    @OneToOne
    private Pessoa pessoa;

    @OneToMany(mappedBy="odontograma", fetch=FetchType.EAGER, orphanRemoval=true, cascade=CascadeType.ALL)
    private Set<Dente> dentes;

	public Set<Dente> getDentes() {
		if (dentes == null)
			dentes = new HashSet<Dente>();
        return dentes;
    }

    public void setDentes(Set<Dente> dentes) {
		this.dentes = dentes;
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Odontograma other = (Odontograma) obj;
		if (id != other.id)
			return false;
		return true;
	}
}