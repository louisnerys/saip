package br.com.saip.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.model.DualListModel;

import br.com.saip.dao.CaracteristicaDao;
import br.com.saip.dao.Dao;
import br.com.saip.dao.PessoaDao;
import br.com.saip.model.Caracteristica;
import br.com.saip.model.Dente;
import br.com.saip.model.Face;
import br.com.saip.model.Odontograma;
import br.com.saip.model.Pessoa;

@ManagedBean
@ViewScoped
public class OdontogramaBean extends GenericBean<Pessoa> implements
		Serializable {
	
	@ManagedProperty(value="#{loginBean}")
	private LoginBean loginBean;

	private static final long serialVersionUID = 1L;

	private Odontograma odontograma;

	private Face face;

	private Dente dente;

	private DualListModel<Caracteristica> caracteristicas;

	private boolean ausente;

	private String observacao;

	@Override
	public List<Pessoa> getLista() {
		if(loginBean.isUsrLegista())
			setLista(PessoaDao.getOdontogramasCadaveres());
		else if(loginBean.isUsrDentista()) {
			setLista(PessoaDao.getOdontogramasDesaparecidos(loginBean.getUsuario().getDentista()));
		}
		return this.lista;
	}

	@Override
	public void prepararAdicionar() {
		// Não existe
		setObjeto(null);
	}

	@Override
	public void prepararAlterar() {
		super.prepararAlterar();

		if (getObjeto().getOdontograma() == null) {
			getObjeto().setOdontograma(new Odontograma());
		}

		setOdontograma(getObjeto().getOdontograma());
		getOdontograma().setPessoa(getObjeto());
	}

	public void prepararAlterarDente(Long param) {
		setDente(param.intValue());

		setAusente(dente.isAusente());
		setObservacao(dente.getObservacao());
	}

	public void prepararAlterarFace(Long param) {
		setFace(param.intValue());
		
		List<Caracteristica> source = new CaracteristicaDao().list();
		List<Caracteristica> target = new ArrayList<Caracteristica>(face.getCaracteristicas());
		
		setCaracteristicas(new DualListModel<Caracteristica>(source, target));
	}

	public void confirmarDente() {
		dente.setAusente(ausente);
		dente.setObservacao(observacao);
		dente.setNumero(dente.getNumero());
		
		if (ausente)
			dente.getFaces().clear();
		else if (dente.getFaces().isEmpty())
			getOdontograma().getDentes().remove(dente);
	}

	public void confirmarFace() {
		if(getCaracteristicas().getTarget().isEmpty())
			dente.getFaces().remove(face);
		else
			face.setCaracteristicas(new HashSet<Caracteristica>(getCaracteristicas().getTarget()));
	}
	
	@Override
	public void confirmar(ActionEvent actionEvent) {
		try
    	{
	    	new Dao<Odontograma>(){}.update(getOdontograma());
    	}
    	catch( Exception e )
    	{
    		e.printStackTrace();
    		FacesContext.getCurrentInstance().addMessage(null,
    				new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
    						"Ocorreu um erro ao processar."));
    		return;
    	}
    	FacesContext.getCurrentInstance().addMessage(null,
    			new FacesMessage(FacesMessage.SEVERITY_INFO, "",
    					"Opera&ccedil;&atilde;o realizada com sucesso!"));
	}

	public boolean isTemFace5() {
		return Arrays.asList(
				new Integer[] { 14, 15, 16, 17, 18, 24, 25, 26, 27, 28, 54, 55, 64, 65,
								84, 85, 74, 75, 44, 45, 46, 47, 48, 34, 35, 36, 37, 38
				}).contains(getDente());
	}

	public Odontograma getOdontograma() {
		return odontograma;
	}

	public void setOdontograma(Odontograma odontograma) {
		this.odontograma = odontograma;
	}

	public int getFace() {
		if(face == null)
			return 0;
		
		return face.getNumero();
	}

	public void setFace(int face) {
		boolean jaTem = false;
		
		for(Face f : dente.getFaces()) {
			if(f.getNumero() == face) {
				this.face = f;
				jaTem = true;
				break;
			}
		}
		
		if(!jaTem) {
			this.face = new Face();
			this.face.setNumero(face);
			this.face.setDente(dente);
			dente.getFaces().add(this.face);
		}
	}

	public DualListModel<Caracteristica> getCaracteristicas() {
		if(caracteristicas == null)
			caracteristicas = new DualListModel<Caracteristica>(Collections.<Caracteristica> emptyList(), Collections.<Caracteristica> emptyList());
		caracteristicas.setSource(new CaracteristicaDao().list());
		return caracteristicas;
	}

	public void setCaracteristicas(DualListModel<Caracteristica> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public int getDente() {
		if(dente == null)
			return 0;
		
		return dente.getNumero();
	}

	public void setDente(int dente) {
		boolean jaTem = false;
		
		for(Dente d : getOdontograma().getDentes()) {
			if(d.getNumero() == dente) {
				this.dente = d;
				jaTem = true;
				break;
			}
		}
		
		if (!jaTem) {
			this.dente = new Dente();
			this.dente.setNumero(dente);
			this.dente.setOdontograma(getOdontograma());
			getOdontograma().getDentes().add(this.dente);
		}
	}
	
	public String getImagemDente(Long selecao) {
		String library = "vazio";
		
		if (odontograma != null)
			for (Dente d : odontograma.getDentes()) {
				if (d.getNumero() == selecao.intValue()) {
					if (d.isAusente())
						library = "ausente";
					else
						library = "dentes";
				}
			}
		
		return library;
	}
	
	public void preencherSuperiores() {
		preencherDentes(11, 12, 13, 14, 15, 16, 17, 18,
						21, 22, 23, 24, 25, 26, 27, 28,
						51, 52, 53, 54, 55,
						61, 62, 63, 64, 65);
	}
	
	public void preencherInferiores() {
		preencherDentes(31, 32, 33, 34, 35, 36, 37, 38,
						41, 42, 43, 44, 45, 46, 47, 48,
						71, 72, 73, 74, 75,
						81, 82, 83, 84, 85);
	}
	
	private void preencherDentes(int... dentes) {
		if(odontograma != null) { // Se não for o momento de carregar a página
			for (Integer dente : dentes) {
				boolean temDente = false;
				for(Dente d : odontograma.getDentes()) {
					if(d.getNumero()==dente) {
						temDente = true;
						if(d.getFaces() != null)
						d.getFaces().clear();
						d.setAusente(true);
						break;
					}
					if(temDente) {
					}
				}
				if(!temDente) {
					Dente d = new Dente();
					d.setAusente(true);
					d.setNumero(dente);
					d.setOdontograma(odontograma);
					odontograma.getDentes().add(d);
				}
			}
		}
	}

	public boolean isAusente() {
		return ausente;
	}

	public void setAusente(boolean ausente) {
		this.ausente = ausente;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public int getTabIndex() {
		return 0;
	}

	public void setTabIndex(int tabIndex) {
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
}