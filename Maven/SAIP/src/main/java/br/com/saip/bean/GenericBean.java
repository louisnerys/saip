package br.com.saip.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import br.com.saip.dao.Dao;

public abstract class GenericBean<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean novo;
    private T objeto;
    protected List<T> lista;
    private List<T> filtro;

    public boolean isNovo() {
        return novo;
    }

    public void setNovo(boolean novo) {
        this.novo = novo;
    }

    public T getObjeto() {
        return objeto;
    }

    public void setObjeto(T objeto) {
        this.objeto = objeto;
    }

    public abstract List<T> getLista();

    public void setLista(List<T> lista) {
        this.lista = lista;
    }

    public List<T> getFiltro() {
		return filtro;
	}

	public void setFiltro(List<T> filtro) {
		this.filtro = filtro;
	}

	public void prepararAdicionar(ActionEvent actionEvent)
    {
        setNovo(true);
        prepararAdicionar();
    }

    public abstract void prepararAdicionar();

    public void prepararAlterar() {
        setNovo(false);
    }

    public  void excluir() {
        new Dao<T>(){}.remove(objeto);
        FacesContext.getCurrentInstance().addMessage(null,
        		new FacesMessage(FacesMessage.SEVERITY_INFO, "",
        				"Opera&ccedil;&atilde;o realizada com sucesso!"));
    }

    public void confirmar(ActionEvent actionEvent) {
    	try
    	{
	    	if(isNovo())
	    		new Dao<T>(){}.save(objeto);
			else
				new Dao<T>(){}.update(objeto);
    	}
    	catch( Exception e )
    	{
    		e.printStackTrace();
    		FacesContext.getCurrentInstance().addMessage(null,
    				new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
    						"Ocorreu um erro ao processar."));
    		return;
    	}
    	FacesContext.getCurrentInstance().addMessage(null,
    			new FacesMessage(FacesMessage.SEVERITY_INFO, "",
    					"Opera&ccedil;&atilde;o realizada com sucesso!"));
    }
}