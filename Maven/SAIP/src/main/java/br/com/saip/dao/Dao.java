package br.com.saip.dao;

import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class Dao<T> implements GenericDao<T> {

    public List<T> list(String query) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        @SuppressWarnings("unchecked")
		List<T> lista = session.createQuery(query).list();
        t.commit();
        session.close();
        return lista;
    }

    public void save(T objeto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.merge(objeto);
        session.flush();
        t.commit();
        session.close();
    }

    public void update(T objeto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.merge(objeto);
        session.flush();
        t.commit();
        session.close();
    }

    public void remove(T objeto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.delete(objeto);
        session.flush();
        t.commit();
        session.close();
    }
}