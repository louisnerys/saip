package br.com.saip.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.saip.dao.CaracteristicaDao;
import br.com.saip.model.Caracteristica;

@ManagedBean
@SessionScoped
public class CaracteristicaBean extends GenericBean<Caracteristica> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Override
    public List<Caracteristica> getLista() {
        setLista(new CaracteristicaDao().list());
        return this.lista;
    }

    @Override
    public void prepararAdicionar() {
        setObjeto(new Caracteristica());
    }
}