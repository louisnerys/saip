package br.com.saip.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.saip.model.Cadaver;
import br.com.saip.model.Dentista;
import br.com.saip.model.Desaparecido;
import br.com.saip.model.Pessoa;
import br.com.saip.util.Constantes;
import br.com.saip.util.HibernateUtil;

public class PessoaDao {

	public static List<Pessoa> getOdontogramasDesaparecidos(Dentista dentista) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.getNamedQuery(
				"Pessoa.getListaOdontogramaDesaparecidoAssociado")
				.setEntity("responsavel", dentista)
				.setInteger("status", Constantes.Status.DESAPARECIDO.ordinal());
		@SuppressWarnings("unchecked")
		List<Pessoa> l = query.list();
		session.close();
		return l;
	}

	public static List<Pessoa> getOdontogramasCadaveres() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.getNamedQuery("Pessoa.getListaOdontogramaCadaver");
		@SuppressWarnings("unchecked")
		List<Pessoa> l = query.list();
		session.close();
		return l;
	}

	public static List<Desaparecido> getDesaparecidosNaoIdentificados() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.getNamedQuery("Pessoa.getListaDesaparecidoNaoEncontrado")
				.setInteger("status", Constantes.Status.DESAPARECIDO.ordinal());
		@SuppressWarnings("unchecked")
		List<Desaparecido> l = query.list();
		session.close();
		return l;
	}

	public static List<Cadaver> getCadaveresNaoIdentificados() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.getNamedQuery("Pessoa.getListaCadaverNaoEncontrado");
		@SuppressWarnings("unchecked")
		List<Cadaver> l = query.list();
		session.close();
		return l;
	}

	public static List<Cadaver> getCadaveresIdentificados() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.getNamedQuery("Pessoa.getListaCadaverIdentificado")
				.setInteger("status", Constantes.Status.IDENTIFICADO.ordinal());
		@SuppressWarnings("unchecked")
		List<Cadaver> l = query.list();
		session.close();
		return l;
	}

	public static List<Cadaver> getCadaveresLiberados() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.getNamedQuery("Pessoa.getListaCadaverIdentificado")
				.setInteger("status", Constantes.Status.LIBERADO.ordinal());
		@SuppressWarnings("unchecked")
		List<Cadaver> l = query.list();
		session.close();
		return l;
	}
}