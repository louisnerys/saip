package br.com.saip.bean;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.saip.model.Foto;

@ManagedBean(name="fotoBean")
@SessionScoped
public class FotoBean implements
		Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Foto foto;
	
	public void processFileUpload(FileUploadEvent uploadEvent) {
		try {
			foto.setImagem(uploadEvent.getFile().getContents());
			foto.setMimeType(uploadEvent.getFile().getContentType());
	        FacesContext.getCurrentInstance().addMessage(null,
	        		new FacesMessage(FacesMessage.SEVERITY_INFO, "",
	        				"Imagem carregada"));
		} catch (Exception e) {
			Logger.getLogger(getClass()).error(e);
		}
	}

	public StreamedContent getImagem(Foto foto) {
		if (foto == null)
	        return new DefaultStreamedContent();
	    else
			return new DefaultStreamedContent(new ByteArrayInputStream(foto.getImagem()), foto.getMimeType(), "view.xhtml");
	}
	
	public void prepararAlterar() {
		setFoto(new Foto());
	}
	
	public static List<Foto> getSetAsList(Set<Foto> set) {
		if(set == null)
			return Collections.emptyList();
		
		List<Foto> fotos = new ArrayList<Foto>(set);
		
		ServletContext sContext = (ServletContext) FacesContext
				.getCurrentInstance().getExternalContext().getContext();
		
		new File(sContext.getRealPath("/temp")).mkdirs();

		for (Foto f : fotos) {
			String nomeArquivo = f.getId() + ".jpg";
			String arquivo = sContext.getRealPath("/temp") + File.separator
					+ nomeArquivo;

			criaArquivo(f.getImagem(), arquivo);
		}
		
		return fotos;
	}

	private static void criaArquivo(byte[] bytes, String arquivo) {
		FileOutputStream fos;

		try {
			fos = new FileOutputStream(arquivo);
			fos.write(bytes);

			fos.flush();
			fos.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public Foto getFoto() {
		return foto;
	}

	public void setFoto(Foto foto) {
		this.foto = foto;
	}
}