package br.com.saip.dao;

import br.com.saip.model.Estado;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class EstadoDao extends Dao<Estado> {
    public Estado getEstado(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Estado)session.load(Estado.class, id);
    }

    public List<Estado> list(){
        return list(" from Estado order by sigla");
    }
}