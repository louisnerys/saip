package br.com.saip.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import br.com.saip.util.Constantes;
import br.com.saip.util.Constantes.Sexo;
import br.com.saip.util.Constantes.TipoUsuario;

@ManagedBean
@ApplicationScoped
public class ConstantesBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public TipoUsuario[] getTiposUsuario() {
		return new TipoUsuario[] {TipoUsuario.IML, TipoUsuario.DENTISTA, TipoUsuario.LEGISTA};
	}
	
	public List<SelectItem> getNomesTiposUsuario() {
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem("", "Selecione"));
		for (TipoUsuario tipo : getTiposUsuario())
			lst.add(new SelectItem(tipo.getLabel(), tipo.getLabel()));
		return lst;
	}
	
	public Sexo[] getSexos() {
		return Sexo.values();
	}
	
	public List<SelectItem> getNomesSexos() {
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem("", "Selecione"));
		for (Sexo sexo : Sexo.values())
			lst.add(new SelectItem(sexo.getLabel(), sexo.getLabel()));
		return lst;
	}
	
	public Constantes.Status[] getStatus() {
		return Constantes.Status.values();
	}
	
	public List<SelectItem> getNomesStatus() {
		List<SelectItem> lst = new ArrayList<SelectItem>();
		lst.add(new SelectItem("", "Selecione"));
		for (Constantes.Status status : Constantes.Status.values())
			lst.add(new SelectItem(status.getLabel(), status.getLabel()));
		return lst;
	}
	
	public List<SelectItem> getNomesBoolean() {
		return Arrays.asList(new SelectItem[] {
				new SelectItem("", "Selecione"),
				new SelectItem("false", "Sim"),
				new SelectItem("true", "Não")
		});
	}
}
