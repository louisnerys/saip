package br.com.saip.dao;

import br.com.saip.model.Caracteristica;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class CaracteristicaDao extends Dao<Caracteristica> {
    public Caracteristica getCaracteristica(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Caracteristica)session.load(Caracteristica.class, id);
    }

    public List<Caracteristica> list(){
        return list(" from Caracteristica");
    }
}
