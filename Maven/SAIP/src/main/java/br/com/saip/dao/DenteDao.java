package br.com.saip.dao;

import br.com.saip.model.Dente;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class DenteDao extends Dao<Dente> {
	
    public Dente getDente(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Dente)session.load(Dente.class, id);
    }

    public List<Dente> list(){
        return list(" from Dente");
    }
}
