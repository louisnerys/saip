package br.com.saip.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import br.com.saip.dao.DentistaPacienteDao;
import br.com.saip.dao.DesaparecidoDao;
import br.com.saip.dao.EstadoDao;
import br.com.saip.model.DentistaPaciente;
import br.com.saip.model.Desaparecido;
import br.com.saip.model.Estado;
import br.com.saip.model.Foto;
import br.com.saip.util.Constantes;

@ManagedBean
@SessionScoped
public class DesaparecidoBean extends GenericBean<Desaparecido> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{fotoBean}")
	private FotoBean fotoBean;
	
	private int estado;
	
	private int responsavel;
	
	private Foto foto;
	
	private Constantes.Sexo sexo;

	@Override
    public List<Desaparecido> getLista() {
        setLista(new DesaparecidoDao().list());
        return this.lista;
    }

    @Override
    public void prepararAdicionar() {
        setObjeto(new Desaparecido());
        setEstado(0);
        setResponsavel(0);
        setSexo(Constantes.Sexo.MASCULINO);
        getObjeto().setStatus(Constantes.Status.DESAPARECIDO);
    }
	
	@Override
	public void prepararAlterar() {
		super.prepararAlterar();
		if(getObjeto().getEstado() == null)
			setEstado(0);
		else
			setEstado(getObjeto().getEstado().getId());
		if(getObjeto().getResponsavel() != null)
			setResponsavel(getObjeto().getResponsavel().getId());
		setSexo(getObjeto().getSexo());
	}
	
	@Override
	public void confirmar(ActionEvent actionEvent) {
		if(estado > 0)
			getObjeto().setEstado(new EstadoDao().getEstado(estado));
		DentistaPaciente resp = null;
		if(responsavel > 0)
			resp = new DentistaPacienteDao().getDentistaPaciente(responsavel);
		getObjeto().setResponsavel(resp);
		getObjeto().setSexo(sexo);
		super.confirmar(actionEvent);
	}
	
	public void adicionaFoto() {
		setFoto(fotoBean.getFoto());
		getObjeto().getFotos().add(getFoto());
		super.confirmar(null);
	}
	
	public void excluirFoto() {
		getObjeto().getFotos().remove(getFoto());
		super.confirmar(null);
	}
	
	public List<Foto> getFotos() {
		if(getObjeto() == null)
			return Collections.emptyList();
		return new ArrayList<Foto>(getObjeto().getFotos());
	}
	
	public void liberado(ActionEvent event) {
		setStatus(Constantes.Status.LIBERADO, event);
	}
	
	public void encontrado(ActionEvent event) {
		setStatus(Constantes.Status.ENCONTRADO, event);
	}
	
	public void setStatus(Constantes.Status status, ActionEvent event) {
		getObjeto().setStatus(status);
		super.confirmar(event);
	}
    
    public List<Estado> getEstados() {
    	return new EstadoDao().list();
    }
    
    public List<DentistaPaciente> getResponsaveis() {
    	return new DentistaPacienteDao().list();
    }

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Constantes.Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Constantes.Sexo sexo) {
		this.sexo = sexo;
	}

	public int getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(int responsavel) {
		this.responsavel = responsavel;
	}
	
	public int getTabIndex() {
		return 0;
	}
	
	public void setTabIndex(int tabInd) { }

	public Foto getFoto() {
		return foto;
	}

	public void setFoto(Foto foto) {
		this.foto = foto;
	}
	
	public void setFotoBean(FotoBean fb) {
		fotoBean = fb;
	}
}