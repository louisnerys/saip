package br.com.saip.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import br.com.saip.util.Constantes;

@Entity
@Table(name="Desaparecido")
public class Desaparecido extends Pessoa implements Serializable{

	private static final long serialVersionUID = 1L;

	@Temporal(javax.persistence.TemporalType.DATE)
    private Date dataDesaparecimento;

	private String dadosEspecificos;
	private String dadosAdicionais;

	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.EAGER)
	@JoinTable(name="Telefone_Desaparecido", 
			   joinColumns=@JoinColumn(name="desaparecido_id"),
			   inverseJoinColumns=@JoinColumn(name="telefone_id"))
	private Set<Telefone> telefones;

	@ManyToOne(cascade=CascadeType.REFRESH)
	private DentistaPaciente responsavel;

	private Constantes.Status status;

	@OneToOne
	private Cadaver encontrado;

    public Cadaver getEncontrado() {
		return encontrado;
	}

	public void setEncontrado(Cadaver encontrado) {
		this.encontrado = encontrado;
	}

	public Set<Telefone> getTelefones() {
		if (telefones == null) telefones = new HashSet<Telefone>();
        return telefones;
    }

    public void setTelefones(Set<Telefone> telefones) {
        this.telefones = telefones;
    }

    public String getDadosAdicionais() {
        return dadosAdicionais;
    }

    public void setDadosAdicionais(String dadosAdicionais) {
        this.dadosAdicionais = dadosAdicionais;
    }

    public String getDadosEspecificos() {
        return dadosEspecificos;
    }

    public void setDadosEspecificos(String dadosEspecificos) {
        this.dadosEspecificos = dadosEspecificos;
    }

    public Date getDataDesaparecimento() {
        return dataDesaparecimento;
    }

    public void setDataDesaparecimento(Date dataDesaparecimento) {
        this.dataDesaparecimento = dataDesaparecimento;
    }

	public DentistaPaciente getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(DentistaPaciente responsavel) {
		this.responsavel = responsavel;
	}

	public Constantes.Status getStatus() {
		return status;
	}

	public void setStatus(Constantes.Status status) {
		this.status = status;
	}
	
	public boolean isDesaparecido() {
		return this.status == Constantes.Status.DESAPARECIDO;
	}
	
	public boolean isEncontrado() {
		return this.status == Constantes.Status.ENCONTRADO;
	}
	
	public boolean isIdentificado() {
		return this.status == Constantes.Status.IDENTIFICADO;
	}
	
	public boolean isLiberado() {
		return this.status == Constantes.Status.LIBERADO;
	}
}