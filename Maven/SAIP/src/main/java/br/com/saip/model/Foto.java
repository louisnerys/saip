package br.com.saip.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Foto")
public class Foto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private int id;

	@Column(columnDefinition="LONGBLOB")
    private byte[] imagem;
	
	private String mimeType;
    
    private String descricao;

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
    public boolean equals(Object obj) {
    	if(this==obj)
    		return true;
    	
    	if (obj == null)
    		return false;
    	
    	if (!(obj instanceof Foto))
    		return false;
    	
    	return getId() == ((Foto) obj).getId();
    }

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
}