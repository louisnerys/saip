package br.com.saip.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import br.com.saip.dao.IMLDao;
import br.com.saip.dao.UsuarioDao;
import br.com.saip.model.Dentista;
import br.com.saip.model.DentistaPaciente;
import br.com.saip.model.IML;
import br.com.saip.model.OdontoLegista;
import br.com.saip.model.Usuario;
import br.com.saip.util.Constantes;
import br.com.saip.util.Constantes.TipoUsuario;

@ManagedBean
@ViewScoped
public class UsuarioBean extends GenericBean<Usuario> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int iml;
	
	private String CFO;
	
	private List<IML> imls;
	
	private Constantes.TipoUsuario tipoUsuario;
	
	private boolean tipoDentista = false;

	public int getIml() {
		return iml;
	}

	public void setIml(int iml) {
		this.iml = iml;
	}

	public List<IML> getImls() {
		imls = new IMLDao().list();
		return imls;
	}

	public void setImls(List<IML> imls) {
		this.imls = imls;
	}

	public boolean isTipoDentista() {
		return tipoDentista;
	}

	public void setTipoDentista(boolean tipoDentista) {
		this.tipoDentista = tipoDentista;
	}

	public String getCFO() {
		return CFO;
	}

	public void setCFO(String cfo) {
		CFO = cfo;
	}

	public Constantes.TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Constantes.TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	public int getTabIndex() {
		return 0;
	}
	
	public void setTabIndex(int tabInd) {}

    @Override
    public List<Usuario> getLista() {
        setLista(new UsuarioDao().list());
        return this.lista;
    }
	
	@Override
	public void prepararAlterar() {
		super.prepararAlterar();
		iml = getObjeto().getIml().getId();
		CFO = getObjeto().getDentista() == null ? null : getObjeto().getDentista().getCFO();
		tipoUsuario = getObjeto().getTipoUsuario();
		mudaTipoUsuario();
	}

    @Override
    public void prepararAdicionar() {
        setObjeto(new Usuario());
        iml = 0;
        CFO = null;
        tipoUsuario = Constantes.TipoUsuario.IML;
        mudaTipoUsuario();
    }
	
	@Override
	public void confirmar(ActionEvent actionEvent) {
		try
    	{
			getObjeto().setIml(IMLDao.getIML(iml));
			getObjeto().setTipoUsuario(getTipoUsuario());
			
			Dentista dentista = null;
			
			if(tipoDentista)
			{
				if(getObjeto().getDentista() != null)
					dentista = getObjeto().getDentista();
				else if(getObjeto().getTipoUsuario().equals(Constantes.TipoUsuario.DENTISTA))
					dentista = new DentistaPaciente();
				else
					dentista = new OdontoLegista();
				dentista.setCFO(getCFO());
			}
			
	    	new UsuarioDao().merge(getObjeto(), dentista);
    	}
    	catch( Exception e )
    	{
    		e.printStackTrace();
    		FacesContext.getCurrentInstance().addMessage(null,
    				new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
    						"Ocorreu um erro ao processar."));
    		return;
    	}
		
    	FacesContext.getCurrentInstance().addMessage(null,
    			new FacesMessage(FacesMessage.SEVERITY_INFO, "",
    					"Opera&ccedil;&atilde;o realizada com sucesso!"));
	}
	
	public void mudaTipoUsuario() {
		setTipoDentista(getTipoUsuario() == TipoUsuario.DENTISTA ||
				getTipoUsuario() == TipoUsuario.LEGISTA);
	}
}