package br.com.saip.bean;

import java.io.Serializable;
import java.security.MessageDigest;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import br.com.saip.dao.Dao;
import br.com.saip.dao.UsuarioDao;
import br.com.saip.model.Usuario;
import br.com.saip.util.Constantes;

@SessionScoped
@ManagedBean(name="loginBean")
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String username;
	private String senha;
	
	private Usuario usuario = null;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		MessageDigest md = null;
    	
    	try
    	{
    		md = MessageDigest.getInstance("SHA");
    		md.update(senha.getBytes("UTF-8"));
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
     
        byte raw[] = md.digest();
        String hash = DatatypeConverter.printBase64Binary(raw);
        
        this.senha = hash;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public void prepararAdicionar() {
		username = "";
		senha = "";
	}
	
	public String logIn() {
		Usuario usuario = UsuarioDao.getUsuarioPorUsername(getUsername());
		if(usuario != null && usuario.getSenha().equals(getSenha()))
		{
			setUsuario(usuario);
			getRequest().getSession().setAttribute("usuario", usuario);
			return "/index.xhtml";
		}
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
						"Usu&aacute;rio/Senha inv&aacute;lidos!"));
		return null;
	}
	
	public String logOut() {
		usuario = null;
		getRequest().getSession().invalidate();
		return "/login.xhtml";
	}
	
	private HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	public boolean isLogado() {
		return usuario != null;
	}
	
	public boolean isUsrAdm() {
		return usuario != null && usuario.getTipoUsuario() == Constantes.TipoUsuario.ADMINISTRADOR;
	}
	
	public boolean isUsrIML() {
		return usuario != null && usuario.getTipoUsuario() == Constantes.TipoUsuario.IML;
	}
	
	public boolean isUsrDentista() {
		return usuario != null && (usuario.getTipoUsuario() == Constantes.TipoUsuario.DENTISTA ||
				usuario.getTipoUsuario() == Constantes.TipoUsuario.LEGISTA);
	}
	
	public boolean isUsrLegista() {
		return usuario != null && usuario.getTipoUsuario() == Constantes.TipoUsuario.LEGISTA;
	}
	
	public void alterarUsuario() {
		new Dao<Usuario>(){}.update(usuario);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "",
						"Dados alterados com sucesso!"));
	}
}