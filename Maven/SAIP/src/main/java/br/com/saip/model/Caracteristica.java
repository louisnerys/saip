package br.com.saip.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="Caracteristica")
public class Caracteristica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private int id;

    private String descricao;

    @ManyToMany(mappedBy = "caracteristicas")
    private List<Face> faces;

    public List<Face> getFaces() {
        return faces;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Caracteristica other = (Caracteristica) obj;
		if (id != other.id)
			return false;
		return true;
	}
}