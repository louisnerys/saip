package br.com.saip.dao;

import java.util.List;

import org.hibernate.Session;

import br.com.saip.model.Cadaver;
import br.com.saip.util.HibernateUtil;

public class CadaverDao extends Dao<Cadaver> {
    public Cadaver getCadaver(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Cadaver)session.load(Cadaver.class, id);
    }

    public List<Cadaver> list(){
        return PessoaDao.getCadaveresNaoIdentificados();
    }
}
