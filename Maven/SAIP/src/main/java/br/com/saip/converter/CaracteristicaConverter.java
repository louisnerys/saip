package br.com.saip.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.saip.dao.CaracteristicaDao;
import br.com.saip.model.Caracteristica;

@FacesConverter(value="caracteristica")
public class CaracteristicaConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return new CaracteristicaDao().getCaracteristica(Integer.parseInt(value));
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return "" + ((Caracteristica)value).getId();
	}

}
