package br.com.saip.dao;

import java.util.List;

public interface GenericDao<T> {
    public void save(T objeto);
    public List<T> list(String query);
    public void remove(T objeto);
    public void update(T objeto);
}
