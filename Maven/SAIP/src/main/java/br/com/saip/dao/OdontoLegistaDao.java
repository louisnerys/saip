package br.com.saip.dao;

import br.com.saip.model.OdontoLegista;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class OdontoLegistaDao extends Dao<OdontoLegista> {
    public OdontoLegista getOdontoLegista(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (OdontoLegista)session.load(OdontoLegista.class, id);
    }

    public List<OdontoLegista> list(){
        return list(" from OdontoLegista");
    }
}
