package br.com.saip.dao;

import br.com.saip.model.Desaparecido;
import br.com.saip.util.Constantes;
import br.com.saip.util.HibernateUtil;

import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.Query;
import org.hibernate.Session;

@NamedQueries({
	@NamedQuery(
			name = "Desaparecido.getIdentificados",
			query = "from Desaparecido where status = :status order by numero") })
public class DesaparecidoDao extends Dao<Desaparecido> {
    public Desaparecido getDesaparecido(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Desaparecido)session.load(Desaparecido.class, id);
    }

    public List<Desaparecido> list(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.createQuery(" from Desaparecido where status = :desaparecido or status = :encontrado order by numero")
				.setInteger("desaparecido", Constantes.Status.DESAPARECIDO.ordinal())
				.setInteger("encontrado", Constantes.Status.ENCONTRADO.ordinal());
		@SuppressWarnings("unchecked")
		List<Desaparecido> l = query.list();
		session.close();
        return l;
    }

	public static List<Desaparecido> getIdentificados() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.getNamedQuery("Desaparecido.getIdentificados")
				.setInteger("status", Constantes.Status.IDENTIFICADO.ordinal());
		@SuppressWarnings("unchecked")
		List<Desaparecido> l = query.list();
		session.close();
		return l;
	}

}
