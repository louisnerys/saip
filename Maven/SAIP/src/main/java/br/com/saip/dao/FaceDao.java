package br.com.saip.dao;

import br.com.saip.model.Face;
import br.com.saip.util.HibernateUtil;

import java.util.List;
import org.hibernate.Session;

public class FaceDao extends Dao<Face> {
    public Face getFace(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Face)session.load(Face.class, id);
    }

    public List<Face> list(){
        return list(" from Face");
    }

}
