package br.com.saip.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.saip.model.Usuario;

public class LoginCheckFilter extends AbstractFilter implements Filter {
	
	private static List<String> permitidas;
	
	private static String contextPath;
	
	private static String resources;

	public void init(FilterConfig fConfig) throws ServletException {
		if(permitidas == null) {
			contextPath = fConfig.getInitParameter("ContextPath");
			resources = fConfig.getInitParameter("resources");
			permitidas = new ArrayList<String>();
			permitidas.add(contextPath + fConfig.getInitParameter("loginActionURI"));
		}
	}

	public void destroy() { }

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		
		if(session.isNew()) {
			doLogin(request, response, req);
			return;
		}
		
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		if(usuario == null && !permitidas.contains(req.getRequestURI()) &&
				!req.getRequestURI().startsWith(contextPath + resources)) {
			doLogin(request, response, req);
			return;
		}
		
		chain.doFilter(request, response);
	}
}