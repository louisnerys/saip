package br.com.saip.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import br.com.saip.model.Caracteristica;
import br.com.saip.model.Dente;
import br.com.saip.model.Face;
import br.com.saip.model.Odontograma;
import br.com.saip.model.Pessoa;

@ManagedBean(name = "denticaoBean")
@RequestScoped
public class DenticaoBean implements Serializable {

	private static final String
		MATCH_ICON = "ui-icon-check",
		NORMAL_ICON = "ui-icon-minus",
		NODE_ICON = "ui-icon-tag";

	private static final long serialVersionUID = 1L;

	public TreeNode getNode(Pessoa este, Pessoa outro) {
		if (este == null || outro == null)
			return new DefaultTreeNode();

		Set<String> listaOdontogramaPossivel = outro.getOdontogramaAsString();

		Odontograma odontograma = este.getOdontograma();
		TreeNode root = new DefaultTreeNode(new Item("root", false), null);

		TreeNode node = new DefaultTreeNode(new Item("Dentes", NODE_ICON), root);

		List<Dente> dentes = new ArrayList<Dente>(odontograma.getDentes());
		Collections.sort(dentes, new Comparator<Dente>() {
			@Override
			public int compare(Dente o1, Dente o2) {
				return new Integer(o1.getNumero()).compareTo(o2.getNumero());
			}
		});

		for (Dente dente : dentes) {
			if (dente.isAusente()) {
				new DefaultTreeNode(new Item("Dente %d (Ausente)",
						dente.getNumero(),
						listaOdontogramaPossivel.contains("0"
								+ dente.getNumero() + "0" + "0000")), node);
				continue;
			}

			TreeNode nodeDente = new DefaultTreeNode(new Item("Dente %d",
					dente.getNumero(), NODE_ICON), node);

			List<Face> faces = new ArrayList<Face>(dente.getFaces());
			Collections.sort(faces, new Comparator<Face>() {
				@Override
				public int compare(Face o1, Face o2) {
					return new Integer(o1.getNumero()).compareTo(o2.getNumero());
				}
			});

			for (Face face : faces) {
				TreeNode nodeFace = new DefaultTreeNode(new Item("Face %d",
						face.getNumero(), NODE_ICON), nodeDente);

				for (Caracteristica caracteristica : face.getCaracteristicas()) {
					new DefaultTreeNode(new Item(caracteristica.getDescricao(),
							listaOdontogramaPossivel.contains("1"
									+ dente.getNumero()
									+ face.getNumero()
									+ String.format("%4d",
											caracteristica.getId()).replace(
											' ', '0'))), nodeFace);
				}
			}
		}

		return root;
	}

	public static class Item {

		private String icon;
		private String descricao;

		public Item() {
		}
		
		public Item(String valor, String icone) {
			descricao = valor;
			icon = icone;
		}

		public Item(String valor, boolean match) {
			descricao = valor;
			icon = match ? MATCH_ICON : NORMAL_ICON;
		}

		public Item(String tipo, int numero, String icone) {
			this(String.format(tipo, numero), icone);
		}

		public Item(String tipo, int numero, boolean match) {
			this(String.format(tipo, numero), match);
		}

		public Item(String tipo, int numero) {
			this(String.format(tipo, numero), false);
		}

		public String getIcon() {
			return icon;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		@Override
		public String toString() {
			return "Item [icon=\"" + icon 
					+ "\", descricao=\"" + descricao + "\"]";
		}
	}
}